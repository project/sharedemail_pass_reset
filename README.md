INTRODUCTION
------------

Choose amongst various strategies of reset passwords in case of multiple users sharing the same email address:

- Only ask for username if multiple records are found for the requested email address (default)

- Only allow username password resets

- Both email and user name required


REQUIREMENTS
------------

None.

INSTALLATION
------------

Install as usual, see [Installing contributed modules
](https://drupal.org/node/895232) for further information.

CONFIGURATION
-------------

1. Navigate to settings form through `Admin > Configuration > People > Shared
E-Mail Password Reset Settings`

   or directly at path `/admin/config/people/shared-email-pass-reset`
