<?php

namespace Drupal\sharedemail_pass_reset;

/**
 * Class SharedEmailPassResetConstants.
 *
 * @package Drupal\sharedemail_pass_reset
 */
abstract class SharedEmailPassResetConstants {

  public const STRATEGY_CONDITIONAL = 'conditional';
  public const STRATEGY_USERNAME = 'username';
  public const STRATEGY_ALL = 'all';

}
